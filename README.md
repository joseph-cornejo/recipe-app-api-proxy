# Recipe-app-api-proxy

NGINX proxy app for our recipe app API

## Usage

### Environement Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_Port` - Port of the app to forward requests to (default `9000`)

